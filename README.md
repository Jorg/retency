# Contexte

Suite au dossier du RAP sur les écrans publicitaires de la RATP (https://antipub.org/dossier-les-cameras-publicitaires-pur-fantasme/), j'ai décidé de créer un petit script permettant de signaler à la société Retency chargée du projet une liste d'adresses MAC (adresse physique d'un périphérique réseau, comme une puce WiFi) au hasard afin de déclarer un maximum de périphériques comme ne devant pas être tracés.

# Explications

Lorsque la page s'affiche, celle-ci génère donc une adresse au hasard et soumet celle-ci à la société.

C'est le navigateur de l'internaute qui pousse les données, ce qui permet d'avoir un grand potentiel et peu de risque d'être contré, car il  s'agira de tas d'adresses IP différentes.

# Installation

L'installation est basique, copiez cette page sur un serveur web et demandez à vos amis, voisins, collègues d'aller la visiter...

